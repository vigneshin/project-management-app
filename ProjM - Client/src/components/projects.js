import axios from "axios";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
const Project = () => {
  const params = useParams();
  const { id } = params;
  const [project, setProject] = useState({});
  const [dev, setDev] = useState([]);
  const [userId, setUserId] = useState();

  function loadDevs() {
    axios.get(`http://localhost:3000/api/user/`).then((res) => {
      setDev(res.data.data);
    });
  }

  function getUserInfo(id) {
    if (!!dev) {
      const user = dev.find((user) => user._id === id);
      return user.firstName;
    }
  }

  function postDev() {
    let formData = new FormData();
    formData.append("projectId", id);
    formData.append("userId", userId);
    axios
      .post(`http://localhost:3000/api/user/assignRole`, formData)
      .then((res) => {
        console.log(res);
      });
  }

  useEffect(() => {
    loadDevs();
    axios.get(`http://localhost:3000/api/project/single/${id}`).then((res) => {
      console.log(res.data.data);
      setProject(res.data.data);
    });
  }, [id]);

  return (
    <>
      <div>
        <h3>Project Details</h3>
        <table
          style={{
            width: "50%",
            margin: "42px",
          }}
          class="table table-bordered"
        >
          <tbody>
            <tr>
              <td>Project Id</td>
              <td>{project.projectId}</td>
            </tr>
            <tr>
              <td>Project Name</td>
              <td>{project.projectName}</td>
            </tr>
            <tr>
              <td>Start Date</td>
              <td>{project.startDate}</td>
            </tr>
            <tr>
              <td>End Date</td>
              <td>{project.endDate}</td>
            </tr>
            <tr>
              <td>Dev Size</td>
              <td>{project.devSize}</td>
            </tr>
            <tr>
              <td>Budget</td>
              <td>{project.budget}</td>
            </tr>
          </tbody>
        </table>

        <h3>Project Members</h3>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            postDev();
          }}
          style={{
            width: "50%",
            margin: "42px",
          }}
        >
          <select
            class="form-select"
            aria-label="Default select example"
            required
            onChange={(e) => setUserId(e.target.value)}
          >
            {dev.map((dev) => {
              return (
                <option
                  value={dev._id}
                >{`${dev.firstName} ${dev.lastName} (${dev.role})`}</option>
              );
            })}
          </select>
          <br />
          <button type="submit" class="btn btn-primary">
            Submit
          </button>
        </form>
        <table
          style={{
            width: "50%",
            margin: "42px",
          }}
          class="table table-bordered"
        >
          <tbody>
            {project.dev
              ? //eslint-disable-next-line
                project.dev.map((match) => {
                  if (match.length > 1) {
                    return (
                      <tr>
                        <td>{getUserInfo(match)}</td>
                      </tr>
                    );
                  }
                })
              : "No Data"}
          </tbody>
        </table>
      </div>
    </>
  );
};

export default Project;
