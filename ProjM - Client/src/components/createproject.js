import styled from "styled-components";
import axios from "axios";
import { useEffect, useState } from "react";
import DateTimePicker from "react-datetime-picker";
import { Link } from "react-router-dom";
const CreateProject = () => {
  const [projectData, setProjectData] = useState({});
  const [endDate, endDateChange] = useState(new Date());
  const [startDate, startDateChange] = useState(new Date());
  const [projectFormData, setProjectFormData] = useState({
    projectId: "",
    projectName: "",
    startDate: startDate,
    endDate: endDate,
    devSize: "",
    dev: [],
    budget: "",
  });

  function handleSubmit(e) {
    e.preventDefault();
    postProjectData();
  }

  function postProjectData() {
    let formData = new FormData();
    formData.append("projectId", projectFormData.projectId);
    formData.append("projectName", projectFormData.projectName);
    formData.append("startDate", startDate);
    formData.append("endDate", endDate);
    formData.append("devSize", projectFormData.devSize);
    formData.append("dev", projectFormData.dev);
    formData.append("budget", projectFormData.budget);
    axios
      .post("http://localhost:3000/api/project/create", formData)
      .then((res) => {
        console.log(res);
        console.log(res.data);
        fetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function fetchData() {
    axios.get("http://localhost:3000/api/project/").then((res) => {
      setProjectData(res.data.data);
      console.log(res.data.data);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const OnChangeValue = (e) => {
    const { value, name } = e.target;
    setProjectFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };
  return (
    <>
      <UsersWrapper>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <form onSubmit={handleSubmit}>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Project Id
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="projectId"
                    type="text"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="UISJ789"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Project Name
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="projectName"
                    type="text"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="Meetly Project"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Developer Size
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="devSize"
                    type="number"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="10"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Project Budget
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="budget"
                    type="number"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="10000"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Start Date
                  </label>
                  <br />
                  <DateTimePicker
                    onChange={startDateChange}
                    value={startDate}
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    End Date
                  </label>
                  <br />
                  <DateTimePicker onChange={endDateChange} value={endDate} />
                </div>
                <center>
                  <button type="submit" class="btn btn-primary">
                    Submit
                  </button>
                </center>
              </form>
            </div>
            <div class="col-md-12">
              {projectData.length > 0 ? (
                <table class="table border ">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Project ID</th>
                      <th scope="col">Project Name</th>
                      <th scope="col">Start Date</th>
                      <th scope="col">End Date</th>
                      <th scope="col">Duration</th>
                      <th scope="col">Developer Size</th>
                      <th scope="col">Budget</th>
                      <th scope="col">Members</th>
                    </tr>
                  </thead>
                  <tbody>
                    {projectData.map((user, index) => (
                      <tr>
                        <th scope="row">{index + 1}</th>
                        <td>{user.projectId}</td>
                        <td>{user.projectName}</td>
                        <td>{user.startDate}</td>
                        <td>{user.endDate}</td>
                        <td>
                          {(new Date(user.endDate) - new Date(user.startDate)) /
                            (1000 * 3600 * 24)}
                        </td>
                        <td>{user.devSize}</td>
                        <td>{user.budget}</td>
                        <td>
                          <Link to={`/projects/${user._id}`}>
                            {user.projectName}
                          </Link>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                "No Data"
              )}
            </div>
          </div>
        </div>
        {JSON.stringify(projectFormData)}
      </UsersWrapper>
    </>
  );
};

export default CreateProject;

const UsersWrapper = styled.div`
  width: 100vh;
  height: 100vh;
`;
