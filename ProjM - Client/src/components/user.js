import styled from "styled-components";
import axios from "axios";
import { useEffect, useState } from "react";

const User = () => {
  const [userData, setUserData] = useState({});
  const [userFormData, setUserFormData] = useState({
    userId: "",
    firstName: "",
    lastName: "",
    role: "Developer",
  });

  function handleSubmit(e) {
    e.preventDefault();
    postUserData();
  }

  function postUserData() {
    let formData = new FormData();
    formData.append("userId", userFormData.userId);
    formData.append("firstName", userFormData.firstName);
    formData.append("lastName", userFormData.lastName);
    formData.append("role", userFormData.role);
    axios
      .post("http://localhost:3000/api/user/register", formData)
      .then((res) => {
        console.log(res);
        fetchData();
      })
      .catch((err) => {
        console.log(err);
      });
  }
  function fetchData() {
    axios.get("http://localhost:3000/api/user/").then((res) => {
      setUserData(res.data.data);
      console.log(res.data.data);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  const OnChangeValue = (e) => {
    const { value, name } = e.target;
    setUserFormData((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });
  };
  return (
    <>
      <UsersWrapper>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <form onSubmit={handleSubmit}>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Developer User Id
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="userId"
                    type="text"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="UISJ789"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    First Name
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="firstName"
                    type="text"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="Vignesh"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Last Name
                  </label>
                  <input
                    onChange={OnChangeValue}
                    name="lastName"
                    type="text"
                    class="form-control"
                    id="exampleFormControlInput1"
                    placeholder="Shetty"
                    required
                  />
                </div>
                <div class="mb-3">
                  <label for="exampleFormControlInput1" class="form-label">
                    Developer Role
                  </label>
                  <select
                    name="role"
                    onChange={OnChangeValue}
                    class="form-select"
                    aria-label="Default select example"
                    required
                  >
                    <option value="Developer">Developer</option>
                    <option value="Tester">Tester</option>
                    <option value="Lead">Lead</option>
                    <option value="Project Manager">Project Manager</option>
                  </select>
                </div>
                <center>
                  <button type="submit" class="btn btn-primary">
                    Submit
                  </button>
                </center>
              </form>
            </div>
            <div class="col-md-6">
              {userData.length > 0 ? (
                <table class="table border">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">User Id</th>
                      <th scope="col">First</th>
                      <th scope="col">Last</th>
                      <th scope="col">Role</th>
                    </tr>
                  </thead>
                  <tbody>
                    {userData.map((user, index) => (
                      <tr>
                        <th scope="row">{index + 1}</th>
                        <td>{user.userId}</td>
                        <td>{user.firstName}</td>
                        <td>{user.lastName}</td>
                        <td>{user.role}</td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              ) : (
                "No Data"
              )}
            </div>
          </div>
        </div>
        {JSON.stringify(userFormData)}
      </UsersWrapper>
    </>
  );
};

export default User;

const UsersWrapper = styled.div`
  width: 100vh;
  height: 100vh;
`;
