import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import CreateProject from "./components/createproject";
import Project from "./components/projects";
import User from "./components/user";
ReactDOM.render(
  <BrowserRouter>
    <Routes>
      <Route path="/users" element={<User />} />
      <Route path="/" element={<CreateProject />} />
      <Route path="/projects/:id" element={<Project />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById("root")
);
