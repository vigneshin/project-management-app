const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const projectSchema = new Schema(
  {
    projectId: { type: String, required: true, unique: true },
    projectName: { type: String, required: true },
    startDate: { type: Date, default: Date.now },
    endDate: { type: Date, default: Date.now },
    devSize: { type: Number, required: true },
    budget: { type: String, required: true },
    dev: { type: [String], required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Project", projectSchema);
