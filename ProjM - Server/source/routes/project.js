const projectRouter = require("express").Router();
const { create, getAll, getOne } = require("../controller/projectController");
const multer = require("multer");
const upload = multer();

projectRouter.get("/single/:id", getOne);
projectRouter.post("/create", upload.none(), create);
projectRouter.get("/", upload.none(), getAll);
module.exports = projectRouter;
