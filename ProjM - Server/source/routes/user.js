const userRouter = require("express").Router();
const {
  register,
  getAllUsers,
  assignRole,
} = require("../controller/devController");
const multer = require("multer");
const upload = multer();

userRouter.post("/register", upload.none(), register);
userRouter.get("/", getAllUsers);
userRouter.post("/assignRole", upload.none(), assignRole);

module.exports = userRouter;
