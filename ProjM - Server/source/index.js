const express = require("express");
const app = express();
const cors = require("cors");
const favicon = require("serve-favicon");
const path = require("path");
const morgan = require("morgan");
const userRouter = require("./routes/user");
const projectRouter = require("./routes/project");
const { Connect } = require("./database/connect");
const corsOptions = {
  origin: ["http://localhost:3000", "http://localhost:3002"],
  optionsSuccessStatus: 200,
};

app.use(cors(corsOptions));
app.use(favicon(path.join(__dirname, "../public", "favicon.ico")));
app.use(morgan("tiny"));
Connect();

app.use("/api/user", userRouter);

app.use("/api/project", projectRouter);

app.get("/", (req, res) => {
  return res.json({
    about: "Project Management Application API",
    version: "1.0.0",
    developer: "Vignesh Shetty",
    host: req.hostname,
  });
});

const PORT = process.env.PORT || 3000;

app.listen(3000, () => {
  console.log(`Server is running on port ${PORT}`);
});
