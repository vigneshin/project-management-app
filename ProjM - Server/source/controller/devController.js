const { customLogger } = require("../log/winston");
const User = require("../database/schema/user");
const Project = require("../database/schema/project");
exports.register = async function (req, res) {
  try {
    const { userId, firstName, lastName, role } = req.body;
    if (!userId || !firstName || !lastName || !role) {
      return res.status(400).json({
        status: "fail",
        message: "Please provide all required fields!",
      });
    }
    const checkBase = await User.findOne({ userId });
    if (!!checkBase) {
      return res.status(400).json({
        status: "fail",
        message: "User already exists!",
      });
    }
    const user = new User({
      userId,
      firstName,
      lastName,
      role,
    });
    const result = await user.save();
    return res.status(201).json({
      status: "success",
      message: "User created successfully!",
      data: result,
    });
  } catch (err) {
    customLogger.error(err.message, err);
    return res.status(500).json({
      success: false,
      message: "Server Error",
    });
  }
};

exports.getAllUsers = async function (req, res) {
  try {
    const result = await User.find();
    return res.status(200).json({
      status: "success",
      message: "Users fetched successfully!",
      data: result,
    });
  } catch (err) {
    customLogger.error(err.message, err);
    return res.status(500).json({
      success: false,
      message: "Server Error",
    });
  }
};

exports.assignRole = async function (req, res) {
  try {
    const { projectId, userId } = req.body;
    if (!projectId || !userId) {
      return res.status(400).json({
        success: false,
        message: "Please provide all required fields!",
      });
    }
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({
        success: false,
        message: "User not found!",
      });
    }
    const project = await Project.findById(projectId);
    if (!project) {
      return res.status(404).json({
        success: false,
        message: "Project not found!",
      });
    }
    project.dev.push(userId);
    await project.save();
    return res.status(200).json({
      status: "success",
      message: "User assigned successfully!",
    });
  } catch (err) {
    customLogger.error(err.message, err);
    return res.status(500).json({
      success: false,
      message: "Server Error",
    });
  }
};
