const { customLogger } = require("../log/winston");
const Project = require("../database/schema/project");
exports.create = async (req, res) => {
  try {
    const { projectId, projectName, startDate, endDate, devSize, budget, dev } =
      req.body;
    if (
      !projectId ||
      !projectName ||
      !startDate ||
      !endDate ||
      !devSize ||
      !budget
    ) {
      return res.status(400).json({
        message: "Please fill all the fields",
      });
    }
    //TODO: PROJECT NAME SHOULD BE UNIQUE
    const checkBase = await Project.findOne({ projectId });
    if (!!checkBase) {
      return res.status(400).json({
        message: "Project already exists",
      });
    }
    const project = new Project({
      projectId,
      projectName,
      startDate,
      endDate,
      devSize,
      budget,
      dev,
    });
    const newProject = await project.save();
    return res.status(201).json({
      message: "Project created successfully",
      data: newProject,
    });
  } catch (err) {
    customLogger.error(err.message, {
      timestamp: new Date().toISOString(),
      level: "error",
      message: err.message,
      stack: err.stack,
    });
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
    });
  }
};
exports.getOne = async (req, res) => {
  try {
    if (!req.params.id) {
      return res.status(400).json({
        message: "Please provide project id",
      });
    }
    const project = await Project.findById(req.params.id);
    if (!project) {
      return res.status(404).json({
        message: "Project not found",
      });
    }
    return res.status(200).json({
      message: "Project fetched successfully",
      data: project,
    });
  } catch (err) {
    customLogger.error(err.message, {
      timestamp: new Date().toISOString(),
      level: "error",
      message: err.message,
      stack: err.stack,
    });
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
    });
  }
};

exports.getAll = async (req, res) => {
  try {
    const projects = await Project.find();
    return res.status(200).json({
      message: "Projects fetched successfully",
      data: projects,
    });
  } catch (err) {
    customLogger.error(err.message, {
      timestamp: new Date().toISOString(),
      level: "error",
      message: err.message,
      stack: err.stack,
    });
    return res.status(500).json({
      status: 500,
      message: "Internal server error",
    });
  }
};
